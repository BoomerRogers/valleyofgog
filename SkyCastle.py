##Very Beginning intro and global variables.

##Music
import sys, pygame, pygame.mixer
pygame.init()

pygame.mixer.music.load('start.mp3')
pygame.mixer.music.play(-1)


##about
def about():
	created = """\n\n 				\\\\The Valley of Magog//
			Created by: Face_McGace (Herman Rogers)
		    July 2012//Game is Distributed under the GPL
				 'For All and None'\n"""
	print created
about()
title = 'In the land of Magog... where the Demon plays...\n'
width = 90
title = title.center(width)
print '\n'
print title
name = raw_input('\nTo Begin Enter Your Name: ')
raw_input('\nIn this game you\'re provided with prompts that denote choices. These choices are always inputed with a numerical option (1) or capital letter (i.e. Door not door). To see these instructions or an about just enter I or A. \nEnjoy!!\n<Press Enter>')
trigger = False 
##this is a global variable that allows the player to enter a room of their choice, ##completed a puzzle, and continue to the second room.


def escape(): #Spoiler! Correct puzzle is (3,1) (1,2) (2,3)
	pygame.mixer.music.load('start.mp3')
	pygame.mixer.music.play(-1)
	raw_input("""\nHorrified you realize you're in the courtyard of this stone prison. As you take in your surroundings you start to see that you're surrounded by ancient ,lush, vegetation. The distant sound of birds faintly brings back memories...Overhead the Eternal Sun, bringing some semblance of comfort, stands directly at noon as it has been for the past millennia.\n<Press Enter>""")
	raw_input ("""\nYou look around and see three mirrors facing different directions, (1)Triangle of the North, (2)Circle of the South and (3)Line of the West... Eerily they seem to be drawn to a obelisk at the center...\n<Press Enter>""")
	raw_input("""\nBelow your feet you discern a map spanning the room's width. You notice the marks lead to the central obelisk creating a path to...somewhere. The symbols on the floor represent a (1)River, (2)House, and (3)Sun.\n<Press Enter>""")
	next = raw_input('\nDo you want to Check the room first or see if the Mirrors will move?\n > ')
	if next == 'Check':
		print '\nYou walk around the rooms large pillars checking doorways that have long since been caved in.\n*A thought that has sliently been eating at your consciousness abruptly comes to the fore*\n"Gods... how long have I been asleep here...days? years?...longer?"\nThe last thing remembered was seeing the city...Samarkand. For some reason you began to run...committing a muder againt...then it was the forest..damp, cold, forgotten.\n'
		raw_input('As your fingers trace the carvings in the wall, you come upon the three symbols of unity, a River in the shape of the Line, the Sun symbolized as a Circle, and the last one faded beyond recognition...no doubt from existing in this moist pit for countless epoch\'s.\n<Press Enter>')
		next2 = raw_input('\nContinuing your walk around the room you become entranced with the smooth, flacid obsidian face of the obelisk.\n\nShould you continue to Check the room or attempt to move the Mirrors? > ')
		if next2 == 'Check':
			print '\n\nWhile continuing around the room you realize that a name, oddly familiar, is scratched on the wall..Gog..\n*My name is %s...right?*\nSlowly the symbols become unreadable, squinting you realize the language is incomprehensible. But this room, it too, begins to feel familiar. The feeling of emptiness, of dread, pushes down on you...without realizing what you were doing you begin to push the mirrors into place\n' %name
			mirrors()
		if next2 == 'Mirrors':
			mirrors()
		else:
			print '\nOnce again you fall asleep...man you sure are lazy!\n'
			escape()
	elif next == 'Mirrors':
		mirrors()
	elif next == 'A':
		about()
	else:
		print 'You decided to neither Check the room or use the Mirrors...You lay down and quickly succumb to sleep...as you wake up...'
		escape()


def mirrors():
	pygame.mixer.music.load('dawn.mp3')
	pygame.mixer.music.play(-1)
	global trigger
	triangle = False
	circle = False
	path = False
	print """\nUse the 'Move x to x' command to move a mirror (1,2,3) to the correct symbol (1,2,3) on the floor to proceed, press S to see the symbols again."""
	while 2:
		puzzle = raw_input('> ')
		if puzzle == 'Move 1 to 1':
			print 'A Triangle cannot match the flow of the River.'
		elif puzzle == 'Move 1 to 2' and not triangle:
			print '<<<You Hear a Loud Click>>> A Triangle Represents the Shelter we 	Live in. Mirror is in place(1. Triangle, 2. House). A piece of parchment breaks off ----> "The Name to enter ".'
			triangle = True
		elif puzzle == 'Move 1 to 3':
			print 'A Triangle cannot match the raw power of the Sun'
		elif puzzle == 'Move 2 to 1':
			print 'A Circle cannot flow as a River'
		elif puzzle == 'Move 2 to 2':
			print 'A Circle does not provide the structure of a House'
		elif puzzle == 'Move 2 to 3' and not circle:
			print '<<<You Hear a Loud Click>>> The Circle represents the Power of our Eternal Sun. Mirror is in place(2. Circle, 3. Sun ). A piece of parchment breaks off ----> "the Portal of  ".'
			circle = True
		elif puzzle == 'Move 3 to 1' and not path:
			print '<<<You Hear a Loud Click>>> The Line symbolizes the Heroes Journey which leads us onto a path of Tragedy and Joy. Mirror is in place(3. Line 1. Path). A piece of partchment breaks off ----> " Darkness, is Gog."'
			path = True
		elif puzzle == 'Move 3 to 2':
			print 'The Line cannot support those who wish to stay.'
		elif puzzle == 'Move 3 to 3':
			print 'Though the Line lies in Infinity as does the Sun, a Line had no end or beginning.'
		elif puzzle == 'Gog' and triangle == True and circle == True and path == True:
			if trigger == True:
				puzzles_unlocked()
				break
			else:
				print '\nYou hear a voice whisper, %s, we were once... Suddenly the ground starts to gently shift as debri clears from a metal door directly ahead. Wondering what is contained within, you walk up to it.\n' %name
				trigger = True
				door()
				break
		elif puzzle == 'S':
			puzzle_instructions()
		elif puzzle == 'I':
			instructions()
		elif puzzle == 'A':
			about()
		else:
			print 'Your mind is weakened by the puzzle, but you must push on to escape this place.'


def puzzles_unlocked():
	pygame.mixer.music.load('Constancy.mp3')
	pygame.mixer.music.play(-1)
	raw_input("""\nAs the last piece aligns the Sun overhead shines brighter becoming blinding, you hold your hand up to block what light you can, fear begins to take you as the threat of being enveloped in the Sun's wrath intensifies.\n\n*The ground starts to tremble*\n\nYou can feel the air, the light, slowly draw into the middle of the courtyard. It starts to pull you towards it.\n<Press Enter>""")
	raw_input("""\nSuddenly a Portal bursts out of nowhere, the sheer gravity drains all color from the world, you feel your life fading quickly.\n<Press Enter>\n""")
	print '"%s, you have come back...to the place where you belong...have you forgotton who you are? Whom you used to serve? The very essence that makes us what we are?"\n'%name
	raw_input("""\nThe darkness inside strengthens its pull not only on your body but your mind. You crash into the ground, clinging to a nearby stone pillar, ropes once peacefully resting on it flailing towards to portal. Suddenly all your thoughts focus on the dark gateway and what it contains..."Does it contain who I am?? Where I come from?"\n\n....Do you go Into the portal? or End your life before you become something....else?\n<Press Enter>\n""")
	while 1:
		next = raw_input('Into/End > ')
		if next == 'Into':
			raw_input ('You let go of your mind and body, falling from the rope. The dark figure, somehow familiar, embracing you as you fall into the portal, falling into infinity, into the twilight and dawn of time\n<Press Enter>\n')
			print '.'* 30
			outro()
			break
		if next == 'End':
			raw_input ('You grab on to a nearby rope hanging off the pillar and tie it around your neck...you let go of the rope and fall into eternal bliss\n<Press Enter>\n')
			print '.' * 30
			outro()
			break
		else:
			print 'The darkness is blinding, you feel your mind numb but you must make the decision.'


##Left Swings Alone, Center connects to Left, Right connects to center. All connected ##disks rotate right.
def door():
	pygame.mixer.music.load('start.mp3')
	pygame.mixer.music.play(-1)
	PositionC = False
	PositionR = False
	PositionL = False
	Disk_Left = False
	Disk_Center = False
	Disk_Right = False
	raw_input("""\nYou step warily to the rusted metal door and push gently. The hinges creak in protest.\n\n*I wonder when the last time this door was used..*\n\nYou find yourself in a brightly lit room, the faint sound of water humming in the air. Looking up you notice a waterfall cascading through a large rupture in the ceiling, pouring sunlight into the room. After careful inspection you notice no sign of a person or any trace of where the voice could of came from.\n<Press Enter> """)
	raw_input("""\n\nAs you walk up to the waterfall you notice different streams full of color cascading down the central stone pillar. One lightly touched blue, one flecked with gold, and the last shining a pure silver enhanced by the rays of the Sun. You slowly immerse your hands into the cool liquid...\n<Press Enter> """)
	raw_input("""\nYou notice three rotating plates underneath each stream of water. It seems that each disc must be rotated towards the center (North)...but they are interconnected somehow.\n<Press Enter> """)
	raw_input ("""\nThe Left disk(L) faces North(N), the Center disk(C) faces East(E), and the Right disk(R) faces West(W). Use the "Move x to x" command to move the L, C, R disks to the N, E, W, positions. Remember changing one disk will change another! Press (S) to see instructions.\n<Press Enter> """)
	while 1:
		global trigger
		move = raw_input('> ')
		if move == 'Move L to N':
			print 'You moved the Left disk North'
			PositionL = False
		elif move == 'Move L to W':
			print 'You moved the Left disk West'
			PositionL = False
		elif move == 'Move L to E':
			print 'You moved the Left disk East'
			PositionL = False
		elif move == 'Move L to S':
			print 'You moved the Left disk South'
			PositionL = True
		elif move == 'Move C to N':
			print 'You moved the Center disk North, the Left Disk rotated one position.'
			PositionL = False
		elif move == 'Move C to W':
			print 'You moved the Center disk West, the Left Disk rotated clockwise one notch.'
			PositionC = True
			if PositionL == True:
				Disk_Left = True
			else:
				PositionL = False
		elif move == 'Move C to E':
			print 'You moved the Center disk East'
			PositionC = False
		elif move == 'Move C to S':
			print 'You moved the Center disk South'
			PositionC = False
		elif move == 'Move R to N':
			print 'You Moved the Right disk North, the Center Disk rotated clockwise one notch.'
			PositionR = True
			Disk_Right = True
			if PositionC == True:
				Disk_Center = True
				print 'All three streams collect in the center\n<Press Enter>'
			else:
				PositionC = False
		elif move == 'Move R to W':
			print 'You Moved the Right disk West, the Center Disk rotated clockwise one notch.'
			PositionR = False
			Disk_Right = False
		elif move == 'Move R to E':
			print 'You Moved the Right Disk East'
			PositionR = False
			Disk_Right = False
		elif move == 'Move R to S':
			print 'You Moved the Right Disk South'
			PositionR = False
			Disk_Right = False
		elif move == '' and Disk_Right == True and PositionR == True and PositionC == True and Disk_Center == True and PositionL == True and Disk_Left==True:
			if trigger == True:
				puzzles_unlocked()
				break
			else:
				trigger = True
				raw_input ('The water begins to slowly drains from the pool in the center, in it glows a soft light surrounding your body. A door opens at the other end of the room, light floods in from what looks to be a courtyard...the faint sound of birds can be heard. You walk through the open doorway, what should be your freedom from this place.\n<Press Enter>\n')
				escape()
				break
		elif move == 'S':
			puzzle_instructions2()
		elif move == 'I':
			instructions()
		elif move == 'A':
			about()
		else:
			print 'You splash water on your face and return to the disks.'



def intro():
	pygame.mixer.music.load('dawn.mp3')
	pygame.mixer.music.play(-1)
	global name
	switch = True
	title = "<<<The Valley of Magog>>>"
	width = 90
	title = title.center(width)
	raw_input("""\n\n\nIn the Anatolia Highlands there lies a valley....\nA valley where mists cover the land, a land struck by the gods with eternal sunshine.\nThey say the gods keep a wary watch....for what is contained within....within the Valley...\n<Press Enter>""")
	print title
	wind = pygame.mixer.Sound('wind.wav')
	wind.play(1)
	raw_input("""\n\n**You're walking through seemingly endless green fields. You can feel the sun on your face, the wind gently flows through your hair. Looking out among the horizon you see a city...it's so far away. Then you see it, the dark figure standing in the shade of a tree...gesturing an invite, its dark gaze falls on you....you can hear His voice in your head...**\n<Press Enter>""")
	raw_input("""\nSlowly you open your swollen eyes...you find yourself palming a cold damp floor.\n*cough*(water scatters from your nostrils)\n\n\"Where am I? How long have I been asleep? Who...who was that...shade?"\n<Press Enter>""")
	while 1:
		if switch == True:
			print '\nThen a whisper reverberates in the air.\n"%s, I..know what you are..I can feel you..."' %name
			raw_input ('<Press Enter>')
			raw_input("""\nThe foggy retreat of the dream world suddenly snaps away.\n*struggling to your feet*\n\nYou look around desperately to find yourself in a dark wet room.\n"Who's there!" you yell...no response.\nAs your eyes adjust to the dark the smell of mold permeates the air, a dampness so thick you can feel it entering your lungs.\nIn front of you is a door undoubtedly where the voice came from, an almost pulsating vibration clouds your vision. Who's in there? You wonder...\n<Press Enter>""")
		next = raw_input('\nShould you try the Door or Escape this place?\n> ')
		if next == 'Escape':
			raw_input ("""\nIgnoring what must be through the door ahead you stumble backwards to get away from Him, you walk through an open archway leading outside.\nYou step into sunlight...\n*breathing in the fresh air, and the taste of freedom*\n<Press Enter>""")
			escape()
			break
		if next == 'Door':
			door()
			break
		elif next == 'I':
			switch = False
			instructions()
		elif next == 'A':
			switch = False
			about()
		else:
			switch = False
			print 'Your body falters, almost collapsing. You clasp your temples as your head swarms, you need to decide.'


def outro():
	outro = """\nThank you for playing 'The Vally of Magog'. The game was meant to be a small project using Python. Hopefully more will come in the future!		\n\n--Face_McGace\n\n					   <<<The End>>>\n\n"""
	print outro
	exit(0)
#instructions to help out throughout the game
def instructions():
	instructions = '\nIn this game you\'re provided with prompts which denote choices. These choices are always inputed numerically (1) or with a capital letter (i.e. Door not door). To see these instructions or an about just press I or A. Enjoy!!\n<Press Enter>'
	print instructions


def puzzle_instructions():
	instructions = 'Use the "Move x to x" command to move a puzzle piece. Match the Mirrors with the Floor Symbols. Mirrors = 1. Triangle, 2. Circle, 3. Path, Symbols = 1. River, 2. House, 3. Sun.'
	print instructions


def puzzle_instructions2():
	instructions = '\nThe Left disk(L) faces North(N), the Center disk(C) faces East(E), and the Right disk(R) faces West(W). Use the "Move x to x" command to move the L, C, R disks to the N, E, W, positions. Remember changing one disk will change another! Press (S) to see this again.\n<Press Enter>\n'
	print instructions

intro()